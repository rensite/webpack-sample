/* src/app.js */

// Styles
require('normalize.css');
require('bootstrap');
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import './assets/styles/_app.scss';

$(document).ready(() => {
  console.log('Ready!');

  require('./assets/scripts/index');
});
